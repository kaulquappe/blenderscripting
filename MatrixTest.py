#######################################################################################################################
# Simple example that shows how to use a matrix in order to tranlate, scale and rotate a object in blender.
# It also shows how to create an object from scratch and link it to the scene. 
# @see: https://blender.stackexchange.com/questions/120672/how-to-apply-transformations-to-an-object-using-the-world-matrix
# @see: https://docs.blender.org/api/blender_python_api_2_71_release/mathutils.html#mathutils.Matrix
#
######################################################################################################################

import bpy
from mathutils import Matrix


##########################################################################
def deleteAll():
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.delete(use_global=True)

##########################################################################
def applyMatToVerts(obj, mat):
    for vert in obj.data.vertices:
        vert.co = mat * vert.co
######################################################################################################################
# MAIN

deleteAll()

myCube = bpy.ops.mesh.primitive_cube_add(radius=1, location = (0,0,0))

scaleMatrix = Matrix.Scale(2, 4, (1,0,0))
applyMatToVerts(myCube, scaleMatrix)


#scale_mat2 = Matrix.Scale(sqrt(2), 4, (0,1,0))
#trans_mat = Matrix.Translation((2, 1, -1))
#rot_mat2 = Matrix.Rotation(radians(90), 4, 'X') 

## ~ applyMatToVerts(origCopy, rot_mat1)
## ~ applyMatToVerts(origCopy, scale_mat1)
## ~ applyMatToVerts(origCopy, scale_mat2)
## ~ applyMatToVerts(origCopy, trans_mat)
## ~ applyMatToVerts(origCopy, rot_mat2)


#applyMatToVerts(origCopy, rot_mat2 * trans_mat * scale_mat1 * scale_mat2 * rot_mat1)

######################################################################################################################
# @see: https://docs.blender.org/api/blender_python_api_2_71_release/mathutils.html#mathutils.Matrix
#
# classmethod Scale(factor, size, axis)
#
#    Create a matrix representing a scaling.
#    Parameters:	
#
#        factor (float) – The factor of scaling to apply.
#        size (int) – The size of the scale matrix to construct [2, 4].
#        axis (Vector) – Direction to influence scale. (optional).
#
#    Returns: A new scale matrix.
#    Return type: Matrix
######################################################################################################################

######################################################################################################################
# @see: https://docs.blender.org/api/blender_python_api_2_71_release/mathutils.html#mathutils.Matrix
#
# classmethod Rotation(angle, size, axis)
#
#    Create a matrix representing a rotation.
#    Parameters:	
#
#        angle (float) – The angle of rotation desired, in radians.
#        size (int) – The size of the rotation matrix to construct [2, 4].
#        axis (string or Vector) – a string in [‘X’, ‘Y’, ‘Z’] or a 3D Vector Object (optional when size is 2).
#
#    Returns: A new rotation matrix.
#    Return type: Matrix
#######################################################################################################################

######################################################################################################################
# @see: https://docs.blender.org/api/blender_python_api_2_71_release/mathutils.html#mathutils.Matrix
#
# classmethod Translation(vector)
#
#    Create a matrix representing a translation.
#    Parameters:	vector (Vector) – The translation vector.
#    Returns:	An identity matrix with a translation.
#    Return type:	Matrix
#######################################################################################################################




#``` https://blender.stackexchange.com/questions/120672/how-to-apply-transformations-to-an-object-using-the-world-matrix
#import bpy
#import bmesh
#from math import radians, sqrt
#from mathutils import Matrix

#def getMat(name, color):
#    oldMat = bpy.data.materials.get(name)
#    if(oldMat != None):
#        bpy.data.materials.remove(oldMat)
#    mat = bpy.data.materials.new(name)
#    mat.diffuse_color = color
#    return mat

#def createObjFromCo(meshName, objName, coList, matName, matColor):
#    bm = bmesh.new()
#    verts = []
#    for co in coList:
#        verts.append(bm.verts.new(co))
#    bm.faces.new(verts)
#    mesh = bpy.data.meshes.new(meshName)
#    obj = bpy.data.objects.new(objName, mesh)
#    bpy.context.scene.objects.link(obj)
#    bm.to_mesh(mesh)
#    mat = getMat(matName, matColor)
#    obj.data.materials.append(mat)
#    return obj

#def applyMatToVerts(obj, mat):
#    for vert in obj.data.vertices:
#        vert.co = mat * vert.co

#origObjName = 'origObj'
#origCopyName = 'origCopyObj'
#toMapObjName = 'toMapObj'

#def createObjs():
#    t = [(-1, -1, -1), (1, 1, -1), (-1, 1, 1), (1, -1, 1)]
#    u = [(2, 2, -1), (2, 2, 3), (0, 0, 1), (4, 0, 1)]
#    orange = (0.8, 0.2, 0.1)
#    green = (0.2, 0.8, 0.2)
#    blue = (0.2, 0.2, 0.8)
#    origObj = createObjFromCo(origObjName+'_data', origObjName, t, '__obj1Mat__', orange)
#    origCopy = createObjFromCo(origCopyName+'_data', origCopyName, t, '__objCopyMat__', blue)
#    toMapObj = createObjFromCo(toMapObjName+'_data', toMapObjName, u, '__obj2Mat__',green)

#createObjs()

#origObj = bpy.data.objects['origObj']
#origCopy = bpy.data.objects['origCopyObj']
#toMapObj = bpy.data.objects['toMapObj']

#rot_mat1 = Matrix.Rotation(radians(45), 4, 'Z') 
#scale_mat1 = Matrix.Scale(sqrt(2), 4, (1,0,0))
#scale_mat2 = Matrix.Scale(sqrt(2), 4, (0,1,0))
#trans_mat = Matrix.Translation((2, 1, -1))
#rot_mat2 = Matrix.Rotation(radians(90), 4, 'X') 

## ~ applyMatToVerts(origCopy, rot_mat1)
## ~ applyMatToVerts(origCopy, scale_mat1)
## ~ applyMatToVerts(origCopy, scale_mat2)
## ~ applyMatToVerts(origCopy, trans_mat)
## ~ applyMatToVerts(origCopy, rot_mat2)


#applyMatToVerts(origCopy, rot_mat2 * trans_mat * scale_mat1 * scale_mat2 * rot_mat1)

#```
