import bpy

from math import radians

##########################################################################
def deleteAll():
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.delete(use_global=True)

##########################################################################
def delete(obj2delete):
    bpy.ops.object.select_all(action='DESELECT')
    bpy.data.objects[obj2delete].select = True
    bpy.ops.object.delete()

##########################################################################
#def delete(obj2delete):
#    bpy.data.objects.remove(bpy.data.objects[obj2delete], True)

##########################################################################
def newCuboid(name, xLen=1.0, yLen=1.0, zLen=1.0):
    bpy.ops.mesh.primitive_cube_add(radius=1, location = (0,0,0))
    bpy.context.active_object.name = name
    bpy.context.active_object.data.name = name
    bpy.context.active_object.scale = (xLen/2.0, yLen/2.0, zLen/2.0)  

##########################################################################
def scale(obj2scale, x=1.0, y=1.0, z=1.0):
    bpy.data.objects[obj2scale].scale = (x/2.0, y/2.0, z/2.0)

##########################################################################
def move(obj2move, x=0.0, y=0.0, z=0.0 ):
    bpy.data.objects[obj2move].location.x += x
    bpy.data.objects[obj2move].location.y += y
    bpy.data.objects[obj2move].location.z += z

##########################################################################
def rotate(obj2Rotate, degX=0.0, degY=0.0, degZ=0.0):
    bpy.ops.object.select_all(action='DESELECT')
    obj = bpy.data.objects[obj2Rotate]
    obj.rotation_euler = (radians(degX), radians(degY), radians(degZ))

##########################################################################
##########################################################################

bpy.context.scene.unit_settings.system="METRIC"

deleteAll()
newCuboid("myCube", 6, 3, 2)
scale('myCube', 2, 2, 2)
move('myCube', 0, 0, 1)
rotate('myCube', 45, 45, 45)


#delete("myCube")

#bpy.data.objects['myCube'].dimensions

# TODO: export stl, import etc.