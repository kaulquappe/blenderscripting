# Blender Script Library
---
Blender scripts and a library used for making 3D objects for 3D printing. Similar to openscad - just that it uses the advantages that Blender offers.

//![Tamdandua Student](pijama_wuerfel.jpg)

## TODOS
---
* derive from bpy.types.object: class bslObject(bpy.types.Object):
* let return object = bpy.data.objects.new(name, mesh) the derived object blsObject
* add new convinience methods like scale(), rotate(), dimensions() etc. to the new blsObject class
* Why does context scale not scale but resize ?
* Set unit to mm ( inch ? )
* Export to stl


## Code Snippets
---


## References
---
* http://sinestesia.co/blog/tutorials/python-cube-matrices/
* https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html
* https://wiki.blender.org/wiki/Building_Blender/Linux/Ubuntu
* https://wiki.blender.org/wiki/Developer_Intro/Advice
* https://wiki.blender.org/wiki/Developer_Intro/Overview

* https://docs.blender.org/api/blender_python_api_2_78a_release/
* https://blender.stackexchange.com/questions/120672/how-to-apply-transformations-to-an-object-using-the-world-matrix
* https://docs.blender.org/api/blender_python_api_2_71_release/mathutils.html#mathutils.Matrix
* http://sinestesia.co/blog/tutorials/python-cube-matrices/
* http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
* https://blenderartists.org/t/local-tranforms/623034/2

## Author:
---
* kaulquappe (https://bitbucket.org/kaulquappe/)
