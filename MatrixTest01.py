import bpy
import bmesh
from math import radians, sqrt
from mathutils import Matrix

def getMat(name, color):
    oldMat = bpy.data.materials.get(name)
    if(oldMat != None):
        bpy.data.materials.remove(oldMat)
    mat = bpy.data.materials.new(name)
    mat.diffuse_color = color
    return mat

def createObjFromCo(meshName, objName, coList, matName, matColor):
    bm = bmesh.new()
    verts = []
    for co in coList:
        verts.append(bm.verts.new(co))
    bm.faces.new(verts)
    mesh = bpy.data.meshes.new(meshName)
    obj = bpy.data.objects.new(objName, mesh)
    bpy.context.scene.objects.link(obj)
    bm.to_mesh(mesh)
    mat = getMat(matName, matColor)
    obj.data.materials.append(mat)
    return obj

def applyMatToVerts(obj, mat):
    for vert in obj.data.vertices:
        vert.co = mat * vert.co

origObjName = 'origObj'
origCopyName = 'origCopyObj'
toMapObjName = 'toMapObj'

def createObjs():
    t = [(-1, -1, -1), (1, 1, -1), (-1, 1, 1), (1, -1, 1)]
    u = [(2, 2, -1), (2, 2, 3), (0, 0, 1), (4, 0, 1)]
    orange = (0.8, 0.2, 0.1)
    green = (0.2, 0.8, 0.2)
    blue = (0.2, 0.2, 0.8)
    origObj = createObjFromCo(origObjName+'_data', origObjName, t, '__obj1Mat__', orange)
    origCopy = createObjFromCo(origCopyName+'_data', origCopyName, t, '__objCopyMat__', blue)
    toMapObj = createObjFromCo(toMapObjName+'_data', toMapObjName, u, '__obj2Mat__',green)

createObjs()

origObj = bpy.data.objects['origObj']
origCopy = bpy.data.objects['origCopyObj']
toMapObj = bpy.data.objects['toMapObj']

rot_mat1 = Matrix.Rotation(radians(45), 4, 'Z') 
scale_mat1 = Matrix.Scale(sqrt(2), 4, (1,0,0))
scale_mat2 = Matrix.Scale(sqrt(2), 4, (0,1,0))
trans_mat = Matrix.Translation((2, 1, -1))
rot_mat2 = Matrix.Rotation(radians(90), 4, 'X') 

# ~ applyMatToVerts(origCopy, rot_mat1)
# ~ applyMatToVerts(origCopy, scale_mat1)
# ~ applyMatToVerts(origCopy, scale_mat2)
# ~ applyMatToVerts(origCopy, trans_mat)
# ~ applyMatToVerts(origCopy, rot_mat2)


applyMatToVerts(origCopy, rot_mat2 * trans_mat * scale_mat1 * scale_mat2 * rot_mat1)
